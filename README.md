# Myserver 

## Introduction

Ce projet permet de déployer un serveur Linux. On y trouve des instructions, des fichiers de configuration et des 

## Instructions 

Clonez le projet dans votre dossier personnel: 
```
git clone https://gitlab.com/Reeqz/myserver.git
```

Entrez maintenant dans le dossier téléchargé:
    cd myserver


Vous pouvez maintenant:  
- [Installer un serveur web](#Installer_un_serveur_web_nginx) 
- Installer un serveur ssh 

## Installer un serveur web (Nginx)

Pour connaitre son adresse ip:
```
ip a 
```
On cherche la ligne qui contient (`ip a |grep inet`).

Ceci est l'adresse de notre ordinateur sur le réseau interne.

A ne pas confondre avec votre adresse ip publique,
qui est l'adresse de votre BOX, accessible depuis Internet.

Pour obtenir cette dernière, tapez "whats is my ip" dans la barre de recherche Google.


##### Connaitre ses noms d'höte

On utiise le fichier `etc/hosts`,
qui fonctionne comme un serveur DNS: il s'agit d'un tableau de correspondance entre des adresses IP et des noms d'hôte.

On vas ainsin pouvoir communiquer avec l'ordinateur en utilisant un choix l'adresse IP ou le nom d'höte.

Ex:
```
127.0.0.1	localhost
127.0.1.1	domani.gn	domani


